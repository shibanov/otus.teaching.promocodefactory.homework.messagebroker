using System.Threading.Tasks;
using MassTransit;
using Otus.Teaching.Pcf.Administration.Core.Services;
using Otus.Teaching.Pcf.EventContracts;

namespace Otus.Teaching.Pcf.Administration.WebHost.EventConsumers
{
    internal class ReceivePromoCodeAdministrationEventConsumer :
        IConsumer<IReceivePromoCodeEvent>
    {
        private readonly PromoCodesService _promoCodesService;

        public ReceivePromoCodeAdministrationEventConsumer(PromoCodesService promoCodesService)
        {
            _promoCodesService = promoCodesService;
        }

        public async Task Consume(ConsumeContext<IReceivePromoCodeEvent> context)
        {
            if (context.Message.PartnerManagerId.HasValue)
                await _promoCodesService.UpdateAppliedPromoCodesAsync(context.Message.PartnerManagerId.Value);
        }
    }
}